package ir.mimrahe.mypoints.database;

import android.content.Context;
import android.util.Log;

import org.greenrobot.greendao.database.Database;

import ir.mimrahe.mypoints.model.DaoMaster;

/**
 * greendao database helper
 */

public class MyDbHelper extends DaoMaster.OpenHelper {
    private final String DEBUG_TAG = MyDbHelper.class.getSimpleName();

    public MyDbHelper(Context context, String name) {
        super(context, name);
    }

    @Override
    public void onUpgrade(Database db, int oldVersion, int newVersion) {
        super.onUpgrade(db, oldVersion, newVersion);
        Log.e(DEBUG_TAG, "old version: " + oldVersion + ", new version: " + newVersion);
    }
}
