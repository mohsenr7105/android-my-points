package ir.mimrahe.mypoints;

import android.app.Application;

import org.greenrobot.greendao.database.Database;

import ir.mimrahe.mypoints.database.MyDbHelper;
import ir.mimrahe.mypoints.model.DaoMaster;
import ir.mimrahe.mypoints.model.DaoSession;

/**
 * application extension
 */

public class MyApp extends Application {
    private DaoSession mDaoSession;

    @Override
    public void onCreate() {
        super.onCreate();

        MyDbHelper helper = new MyDbHelper(this, BuildConfig.DB_NAME);
        Database db = helper.getWritableDb();
        mDaoSession = new DaoMaster(db).newSession();
    }

    public DaoSession getDaoSession(){
        return mDaoSession;
    }
}
