package ir.mimrahe.mypoints.model;

import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.converter.PropertyConverter;

import ir.mimrahe.mypoints.tool.DateHelper;

/**
 * greendao model: points table in sqlite
 */

@Entity(
        nameInDb = "points"
)
public class Point {
    @Id(autoincrement = true)
    private Long id;

    @Property(nameInDb = "title")
    @NotNull
    private String title;

    @Property(nameInDb = "description")
    @NotNull
    private String description;

    @Property(nameInDb = "updated_at")
    @NotNull
    @Convert(converter = JalaliDateConverter.class, columnType = Long.class)
    private JalaliDate updatedAt;

    @Property(nameInDb = "status")
    @NotNull
    @Convert(converter = StatusConverter.class, columnType = Integer.class)
    private Status status;

    public enum Status{
        WORKING(0), CANCELED(1), DONE(2), FAILED(3);

        final int id;

        Status(int id){
            this.id = id;
        }
    }

    @Generated(hash = 640788033)
    public Point(Long id, @NotNull String title, @NotNull String description,
            @NotNull JalaliDate updatedAt, @NotNull Status status) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.updatedAt = updatedAt;
        this.status = status;
    }

    public Point(@NotNull String title, @NotNull String description,
            @NotNull JalaliDate updatedAt, @NotNull Status status) {
        this.title = title;
        this.description = description;
        this.updatedAt = updatedAt;
        this.status = status;
    }

    @Generated(hash = 1977038299)
    public Point() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public JalaliDate getUpdatedAt() {
        return this.updatedAt;
    }

    public void setUpdatedAt(JalaliDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Status getStatus() {
        return this.status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public static class StatusConverter implements PropertyConverter<Status, Integer>{

        @Override
        public Status convertToEntityProperty(Integer databaseValue) {
            if (databaseValue == null)
                return null;
            for (Status status: Status.values()){
                if (status.id == databaseValue){
                    return status;
                }
            }
            return Status.WORKING;
        }

        @Override
        public Integer convertToDatabaseValue(Status entityProperty) {
            return entityProperty == null ? Status.WORKING.id : entityProperty.id;
        }
    }

    public static class JalaliDateConverter implements PropertyConverter<JalaliDate, Long>{

        @Override
        public JalaliDate convertToEntityProperty(Long databaseValue) {
            return DateHelper.g2Jalali(databaseValue);
        }

        @Override
        public Long convertToDatabaseValue(JalaliDate entityProperty) {
            return DateHelper.jalali2G(entityProperty);
        }
    }
}
