package ir.mimrahe.mypoints.model;

import java.util.Locale;

/**
 * jalali date model
 */

public class JalaliDate {
    int mYear, mMonth, mDay;

    public JalaliDate(int year, int month, int day){
        mYear = year;
        mMonth = month;
        mDay = day;
    }

    public int getYear(){
        return mYear;
    }

    public void setYear(int year){
        mYear = year;
    }

    public int getMonth(){
        return mMonth;
    }

    public void setMonth(int month){
        mMonth = month;
    }

    public int getDay(){
        return mDay;
    }

    public void setDay(int day){
        mDay = day;
    }

    public String toString(){
        return String.format(Locale.US,"%1$d/%2$d/%3$d", getYear(), getMonth(), getDay());
    }

    public JalaliDate clone(){
        return new JalaliDate(getYear(), getMonth(), getDay());
    }

    public boolean equals(JalaliDate date){
        if (date == null)
            return false;

        if (date.getYear() != getYear())
            return false;

        if (date.getMonth() != getMonth())
            return false;

        if (date.getDay() != getDay())
            return false;

        return true;
    }
}
