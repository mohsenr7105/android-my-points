package ir.mimrahe.mypoints.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.List;

import ir.mimrahe.mypoints.R;
import ir.mimrahe.mypoints.component.TextViewWithFont;
import ir.mimrahe.mypoints.model.Point;
import ir.mimrahe.mypoints.tool.DateHelper;

/**
 * points list adapter
 */

public class PointsAdapter extends ArrayAdapter<Point> {
    private static class ViewHolder{
        TextViewWithFont textTitle, textDescription, textTime;
        ImageView imageStatus;
    }

    public PointsAdapter(@NonNull Context context,@NonNull List<Point> points) {
        super(context, -1, points);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Point point = getItem(position);
        ViewHolder viewHolder;

        if (convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_point, parent, false);
            viewHolder.textTitle = (TextViewWithFont) convertView.findViewById(R.id.text_item_title);
            viewHolder.textDescription = (TextViewWithFont) convertView.findViewById(R.id.text_item_description);
            viewHolder.textTime = (TextViewWithFont) convertView.findViewById(R.id.text_item_time);
            viewHolder.imageStatus = (ImageView) convertView.findViewById(R.id.image_item_status);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textTitle.setText(point.getTitle());
        viewHolder.textDescription.setText(point.getDescription());
        int days = DateHelper.daysRemains(point.getUpdatedAt());
        if (point.getStatus().equals(Point.Status.WORKING) & days >= 0){
            viewHolder.textTime.setText(String.valueOf(days));
        } else {
            viewHolder.textTime.setText(point.getUpdatedAt().toString());
        }

        int drawableStatus = -1;
        switch (point.getStatus()){
            case WORKING:
                drawableStatus = R.drawable.status_working;
                break;
            case DONE:
                drawableStatus = R.drawable.status_done;
                break;
            case FAILED:
                drawableStatus = R.drawable.status_failed;
                break;
            case CANCELED:
                drawableStatus = R.drawable.status_canceled;
                break;
        }
        viewHolder.imageStatus.setImageDrawable(getContext().getResources().getDrawable(drawableStatus));

        return convertView;
    }
}
