package ir.mimrahe.mypoints.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.util.AttributeSet;

import ir.mimrahe.mypoints.R;

/**
 * edit text with font
 */

public class EditTextWithFont extends AppCompatEditText {
    public EditTextWithFont(Context context) {
        super(context);
    }

    public EditTextWithFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public EditTextWithFont(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs){
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.WithFont);

        if (!typedArray.equals(null)){
            String fontAsset = typedArray.getString(R.styleable.WithFont_typefaceAsset);

            if (!TextUtils.isEmpty(fontAsset)){
                Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/" + fontAsset);

                setTypeface(font);
            }
        }
    }
}
