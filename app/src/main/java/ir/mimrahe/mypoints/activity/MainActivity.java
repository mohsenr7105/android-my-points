package ir.mimrahe.mypoints.activity;

import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import ir.mimrahe.mypoints.MyApp;
import ir.mimrahe.mypoints.R;
import ir.mimrahe.mypoints.adapter.PointsAdapter;
import ir.mimrahe.mypoints.callback.NewPointResponseListener;
import ir.mimrahe.mypoints.component.TextViewWithFont;
import ir.mimrahe.mypoints.fragment.NewBottomSheetFragment;
import ir.mimrahe.mypoints.model.Point;
import ir.mimrahe.mypoints.model.PointDao;
import ir.mimrahe.mypoints.tool.Snack;

public class MainActivity extends AppCompatActivity
        implements NewPointResponseListener{
    private final String TAG_DEBUG = MainActivity.class.getSimpleName();

    ArrayList<Point> points;

    PointsAdapter pointsAdapter;

    @BindView(R.id.holder_activity_main)
    CoordinatorLayout holderActivityMain;

    @BindView(R.id.text_list_info)
    TextViewWithFont textListInfo;

    @BindView(R.id.list_points)
    ListView listPoints;

    @BindView(R.id.fab_new)
    FloatingActionButton fabNew;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        setToolbar();
        initList();
    }

    @OnClick(R.id.fab_new)
    public void onFabClick(View view){
        NewBottomSheetFragment newBottomSheetFragment = new NewBottomSheetFragment();
        newBottomSheetFragment.show(getSupportFragmentManager(), newBottomSheetFragment.getTag());
    }

    @OnItemClick(R.id.list_points)
    public void onListItemClick(AdapterView<?> parent, View view, int position, long id){
        View viewStatus = getLayoutInflater().inflate(R.layout.bottom_sheet_status, null);

        viewStatus.findViewById(R.id.btn_status_working)
                .setOnClickListener(new OnStatusListener(position, Point.Status.WORKING));
        viewStatus.findViewById(R.id.btn_status_done)
                .setOnClickListener(new OnStatusListener(position, Point.Status.DONE));
        viewStatus.findViewById(R.id.btn_status_canceled)
                .setOnClickListener(new OnStatusListener(position, Point.Status.CANCELED));
        viewStatus.findViewById(R.id.btn_status_failed)
                .setOnClickListener(new OnStatusListener(position, Point.Status.FAILED));

        BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(viewStatus);
        dialog.show();
    }

    private class OnStatusListener implements View.OnClickListener{
        private int mPosition;
        private Point.Status mStatus;

        OnStatusListener(int position, Point.Status status){
            mPosition = position;
            mStatus = status;
        }

        @Override
        public void onClick(View v) {
            points.get(mPosition).setStatus(mStatus);
            getApp().getDaoSession().getPointDao().update(points.get(mPosition));
            pointsAdapter.notifyDataSetChanged();
        }
    }

    /*
    inits points list
     */
    private void initList(){
        points = (ArrayList<Point>) getApp().getDaoSession().getPointDao().queryBuilder()
                .orderDesc(PointDao.Properties.UpdatedAt)
                .list();
        pointsAdapter = new PointsAdapter(this, points);
        listPoints.setAdapter(pointsAdapter);
        if (points.isEmpty()) {
            visibleMessageEmpty(true);
            return;
        }
    }

    /*
    show or hide message
     */
    private void visibleMessageEmpty(boolean visible){
        textListInfo.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    /*
    sets toolbar
     */
    private void setToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    /*
    returns application instance
     */
    private MyApp getApp(){
        return (MyApp) getApplication();
    }

    @Override
    public void onNewAdded() {
        Snack.alert(this, holderActivityMain, R.string.msg_new_added);
        points.clear();
        points.addAll(getApp().getDaoSession().getPointDao().queryBuilder()
                .orderDesc(PointDao.Properties.UpdatedAt)
                .list());
        visibleMessageEmpty(false);
        pointsAdapter.notifyDataSetChanged();
    }
}
