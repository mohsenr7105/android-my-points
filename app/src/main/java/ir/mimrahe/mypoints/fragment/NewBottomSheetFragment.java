package ir.mimrahe.mypoints.fragment;


import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ir.mimrahe.mypoints.MyApp;
import ir.mimrahe.mypoints.R;
import ir.mimrahe.mypoints.callback.NewPointResponseListener;
import ir.mimrahe.mypoints.component.EditTextWithFont;
import ir.mimrahe.mypoints.component.TextViewWithFont;
import ir.mimrahe.mypoints.model.JalaliDate;
import ir.mimrahe.mypoints.model.Point;
import ir.mimrahe.mypoints.tool.DateHelper;

/**
 * A simple {@link BottomSheetDialogFragment} subclass.
 */
public class NewBottomSheetFragment extends BottomSheetDialogFragment {
    Unbinder unbinder;
    NewPointResponseListener newPointResponseListener;

    @BindView(R.id.btn_done)
    TextViewWithFont btnDone;

    @BindView(R.id.input_title)
    EditTextWithFont inputTitle;

    @BindView(R.id.input_description)
    EditTextWithFont inputDescription;

    @BindView(R.id.input_year)
    EditTextWithFont inputYear;

    @BindView(R.id.input_month)
    EditTextWithFont inputMonth;

    @BindView(R.id.input_day)
    EditTextWithFont inputDay;

    public NewBottomSheetFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bottom_sheet_new, container, false);
        unbinder = ButterKnife.bind(this, view);

        JalaliDate jalaliDate = DateHelper.nowInJalali();

        inputYear.setText(String.valueOf(jalaliDate.getYear()));
        inputMonth.setText(String.valueOf(jalaliDate.getMonth()));
        inputDay.setText(String.valueOf(jalaliDate.getDay()));

        newPointResponseListener = (NewPointResponseListener) getActivity();

        return view;
    }

    @OnClick(R.id.btn_done)
    public void onDone(View view) {
        String title = inputTitle.getText().toString();
        if (title.isEmpty())
            return;
        String description = inputDescription.getText().toString();
        if (description.isEmpty())
            return;
        String year = inputYear.getText().toString();
        if (year.isEmpty())
            return;
        String month = inputMonth.getText().toString();
        if (month.isEmpty())
            return;
        String day = inputDay.getText().toString();
        if (day.isEmpty())
            return;

        getApp().getDaoSession().getPointDao().insert(
                new Point(
                        title,
                        description,
                        new JalaliDate(
                                Integer.valueOf(year),
                                Integer.valueOf(month),
                                Integer.valueOf(day)
                        ),
                        Point.Status.WORKING
                )
        );
        dismiss();
        newPointResponseListener.onNewAdded();
    }

    private MyApp getApp() {
        return (MyApp) getActivity().getApplication();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        unbinder.unbind();
    }
}
