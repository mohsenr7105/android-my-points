package ir.mimrahe.mypoints.tool;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

/**
 * snacks here
 */

public class Snack {
    public static void alert(Context context, View view, int message){
        show(context, Snackbar.make(view, message, Snackbar.LENGTH_SHORT));
    }

    private static void show(Context context, Snackbar snackbar){
        TextView tv = (TextView) (snackbar.getView()).findViewById(android.support.design.R.id.snackbar_text);
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/iransans.ttf");
        tv.setTypeface(font);
        snackbar.show();
    }
}
