package ir.mimrahe.mypoints.tool;

import java.util.Calendar;
import java.util.GregorianCalendar;

import ir.mimrahe.mypoints.model.JalaliDate;

/**
 * date converting helper
 */

public class DateHelper {
    public static JalaliDate g2Jalali(Long timestamp){
        Calendar gCalendar = Calendar.getInstance();
        gCalendar.setTimeInMillis(timestamp);

        int gregYear = gCalendar.get(Calendar.YEAR);
        int gregMonth = gCalendar.get(Calendar.MONTH) + 1;
        int gregDay = gCalendar.get(Calendar.DAY_OF_MONTH);

        int jalaliYear, jalaliMonth, jalaliDay, gregYear2;

        int[] array = {0,31,59,90,120,151,181,212,243,273,304,334};

        jalaliYear = gregYear <= 1600 ? 0 : 979;
        gregYear -= gregYear <= 1600 ? 621 : 1600;
        gregYear2 = gregMonth > 2 ? gregYear + 1 : gregYear;
        int days = (365 * gregYear) +
                ((gregYear2 + 3) / 4) -
                ((gregYear2 + 99) / 100) +
                ((gregYear2 + 399) / 400) - 80 +
                gregDay + array[gregMonth - 1];
        jalaliYear += 33 * (days / 12053);
        days %= 12053;
        jalaliYear += 4 * (days / 1461);
        days %= 1461;
        jalaliYear += (days - 1) / 365;
        if (days > 365){
            days = (days - 1) % 365;
        }
        jalaliMonth = days < 186 ? 1 + (days / 31) : 7 + ((days - 186) / 30);
        jalaliDay = 1 + (days < 186 ? days % 31 : (days - 186) % 30);

        return new JalaliDate(jalaliYear, jalaliMonth, jalaliDay);
    }

    public static Long jalali2G(JalaliDate jalaliDate){
        int jalaliYear = jalaliDate.getYear();
        int jalaliMonth = jalaliDate.getMonth();
        int jalaliDay = jalaliDate.getDay();

        int gregYear = jalaliYear <= 979 ? 621 : 1600;
        jalaliYear -= jalaliYear <= 979 ? 0 : 979;

        int days = (365*jalaliYear) +(((jalaliYear/33))*8) +((((jalaliYear%33)+3)/4))
                +78 +jalaliDay +((jalaliMonth<7)?(jalaliMonth-1)*31:((jalaliMonth-7)*30)+186);
        gregYear += 400 * (days / 146097);
        days %= 146097;
        if (days > 36524){
            gregYear += 100 * (--days / 36524);
            days %= 36524;
            if (days >= 365) days++;
        }
        gregYear += 4 * (days / 1461);
        days %= 1461;
        gregYear += ((days - 1) / 365);
        if (days > 365){
            days = (days - 1) % 365;
        }
        int gregDay = days + 1;
        int[] array = {
                0, 31,
                ((gregYear % 4 == 0 && gregYear % 100 != 0) || gregYear % 400 == 0) ? 29 : 28,
                31, 30, 31, 30, 31, 31, 30, 31, 30, 31
        };
        int arrayLength = array.length;
        int gregMonth;
        for (gregMonth = 0; gregMonth < arrayLength; gregMonth++){
            if (gregDay <= array[gregMonth]){
                break;
            }
            gregDay -= array[gregMonth];
        }

        return new GregorianCalendar(gregYear, --gregMonth, gregDay).getTimeInMillis();
    }

    public static JalaliDate nowInJalali(){
        return g2Jalali(System.currentTimeMillis());
    }

    public static int daysRemains(JalaliDate future){
        Long diff = jalali2G(future) - System.currentTimeMillis();
        return (int) (diff / (1000 * 60 * 60 * 24));
    }
}
