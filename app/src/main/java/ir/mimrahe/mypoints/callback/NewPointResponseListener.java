package ir.mimrahe.mypoints.callback;

public interface NewPointResponseListener {
    void onNewAdded();
}
